from django.http import JsonResponse

from .models import Attendee, ConferenceVO

from common.json import ModelEncoder

from django.views.decorators.http import require_http_methods

import json

class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]
class AttendeesListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeesDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
    ]

    def get_extra_data(self, o):
        return {"conference": {
            "name": o.conference.name,
            "href": o.conference.get_api_url(),
            }
        }

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeesListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeesDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendees = Attendee.objects.get(id=id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        if request.method == "PUT":
            content = json.loads(request.body)
            try:
                if "conference" in content:
                    conference = Conference.objects.get(id=content["conference"])
                    content["conference"] = conference
            except Conference.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid Conference"},
                    status=400,
                )

            Attendee.objects.filter(id=id).update(**content)

            attendees = Attendee.objects.get(id=id)
            return JsonResponse(
                attendees,
                encoder=AttendeesDetailEncoder,
                safe=False
            )
